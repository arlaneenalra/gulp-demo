'use strict';

var gulp = require('gulp'),
    gutil = require('gulp-util'),
    plumber = require('gulp-plumber'),
    jade = require('gulp-jade'),
    serve = require('gulp-serve'),
    livereload = require('gulp-livereload');

var jade_src = ['./src/**/*.jade'];

var build_target = './public';

gulp.task('jade-compile', function () {
    gulp.src(jade_src)
        .pipe(plumber())
        .pipe(jade({ pretty: true, locals: {}}))
        .pipe(gulp.dest(build_target))
        .pipe(livereload());
});

gulp.task('watch', function() {
    gulp.watch(jade_src, ['jade-compile']);
    livereload.listen();
});

gulp.task('serve', ['watch'], serve(build_target));

gulp.task('default', ['jade-compile'], function () {
    gutil.log('Take a Gulp!');    
});
